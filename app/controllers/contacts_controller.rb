class ContactsController < ApplicationController
  before_action :set_contact, only: %i[ show edit update destroy ]

    def index
        @contacts = Contact.order(created_at: :desc)
    end

    # GET /users/1 or /users/1.json
  def show
    # @contact = Contact.find(params[:id])
    # respond_to do |format|
    #   format.js
    # end
  end

  # GET /users/new
  def new
    @contact = Contact.new
  end

  # GET /users/1/edit
  def edit
  end

  # POST /users or /users.json
  def create
    @contact = Contact.new(contact_params)

    respond_to do |format|
        if @contact.save
            format.js
            format.html { redirect_to contact_url(@contact), notice: 'Kontak was successfully created.' }
            format.json { render :show, status: :created, location: @contact }
            
        else    
            format.js
            format.html { render :new, status: :unprocessable_entity}
            format.json { render json: @contact.errors, status: :unprocessable_entity}
        end
    end
end

     # PATCH/PUT /users/1 or /users/1.json
  def update
    respond_to do |format|
      if @contact.update(contact_params)
        format.html { redirect_to contact_url(@contact), notice: 'Contact was successfully updated.' }
        format.json { render :show, status: :ok, location: @contact }
        format.js
      else
        format.js
        format.html { render :edit, status: :unprocessable_entity}
        format.json { render json: @contact.errors, status: :unprocessable_entity}
        end
    end
end

# DELETE /users/1 or /users/1.json
def destroy
  @contact = Contact.find_by(id: params[:id])
  
  if @contact
    @contact.destroy
    respond_to do |format|
      format.html { redirect_to contacts_url, notice: "Contact was successfully destroyed." }
      format.json { head :no_content }
      format.js
    end
  else
    flash[:error] = "Contact not found"
    redirect_to contacts_url
  end
end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_contact
      @contact = Contact.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def contact_params
        params.require(:contact).permit(:name, :email, :nophone)
      end
  end