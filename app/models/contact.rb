class Contact < ApplicationRecord
    validates :name, presence: true
    validates :email, :nophone, presence: true, uniqueness: true

end
